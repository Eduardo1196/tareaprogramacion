/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import tarea1.model.Puesto;

/**
 *
 * @author Susana
 */
public class ControlBD {
    private EntityManager em;
    private EntityManagerFactory emf;
    private EntityTransaction et;

    public ControlBD()
    {
        emf=Persistence.createEntityManagerFactory("Tarea1PU");
        em=emf.createEntityManager();
        et=em.getTransaction();
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public EntityManagerFactory getEmf() {
        return emf;
    }

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityTransaction getEt() {
        return et;
    }

    public void setEt(EntityTransaction et) {
        this.et = et;
    }
       
    public void refrescar(){
        this.em.clear();
    }
        
}
