/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import proyectoredes.controller.Controller;
import tarea1.model.DiaDto;
import tarea1.model.PuestoDto;
import tarea1.model.RolDto;
import tarea1.service.PuestoService;
import tarea1.service.RolesService;
import tarea1.util.AppContext;
import tarea1.util.FlowController;
import tarea1.util.Formato;
import tarea1.util.Message;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class MantRolesController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNameRol;
    @FXML
    private JFXTextField txtNumeroRol;
    @FXML
    private TableView<DiaDto> tvDias;
    @FXML
    private TableColumn<DiaDto, String> tcNombre;
    @FXML
    private TableColumn<DiaDto, String> tcHoraInicio;
    @FXML
    private TableColumn<DiaDto, String> tcHoraSalida;
    @FXML
    private TableColumn<DiaDto, String> tcHorasTrabajadas;
    @FXML
    private JFXButton btnAgregara;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnAtras;
    @FXML
    private JFXComboBox<PuestoDto> cbPuestos;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnLimpiar;
    @FXML
    private JFXTextField txtBuscar;
    @FXML
    private JFXTextField txtFechaSemana;
     @FXML
    private TableView<RolDto> tvRoles;
    @FXML
    private TableColumn<RolDto, String> tcNombreRol;
    @FXML
    private TableColumn<RolDto, String> tcNumeroRol;

    DiaDto dia;
    RolDto rolDto;
    RolesService sr;
    PuestoService sp;
    PuestoDto puesto;
    ObservableList<DiaDto> dias;
    ObservableList<RolDto> roles;
    ObservableList<PuestoDto> puestos;
    Message mensaje;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
     puestos = FXCollections.observableArrayList();
        dias = FXCollections.observableArrayList();
        sp = new PuestoService();
        sr = new RolesService();
        rolDto = new RolDto();
        //Tabla Roles
        tcNombreRol.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        tcNumeroRol.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNumero().toString()));

        //Tabla Dias
        tcHoraInicio.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorainicio()));
        tcHoraSalida.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorafin()));
        tcHorasTrabajadas.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorastrabajadas()));
        tcNombre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        tvDias.setItems(dias);
        formatos();
    }

    @Override
    public void initialize() {
        if (rolDto.getId() == null) {
            dias = (ObservableList<DiaDto>) AppContext.getInstance().get("Semana");
        } else {
            dias.clear();
            for (DiaDto dia1 : rolDto.getDias()) {
                dias.add(dia1);
            }
        }
        cargarPuestos();
        tvDias.setItems(dias);
    }

    @FXML
    private void agragar(ActionEvent event) {
        if (rolDto.getId() != null) {
            AppContext.getInstance().set("Existe", true);
            AppContext.getInstance().set("SemanaRol", dias);
        } else {
            AppContext.getInstance().set("Existe", false);
        }
        FlowController.getInstance().goView("MantDias");
    }

    @FXML
    private void guardar(ActionEvent event) {
        if (!txtFechaSemana.getText().isEmpty() && !txtNameRol.getText().isEmpty()
                && !txtNumeroRol.getText().isEmpty() && dias != null && cbPuestos.getValue() != null) {
            rolDto.setFechaSemana(txtFechaSemana.getText());
            rolDto.setFjo("I");
            rolDto.setNombre(txtNameRol.getText());
            rolDto.setNumero(Integer.parseInt(txtNumeroRol.getText()));
            rolDto.setDias(dias);
            rolDto.setPuesto(cbPuestos.getValue());
            sr.guardarRoles(rolDto, cbPuestos.getValue());
        } else {
            new Message().show(Alert.AlertType.INFORMATION, "Guardar Rol", "Faltan Algunos espacios, verifique");
        }
    }

    @FXML
    private void eliminar(ActionEvent event) {
        if (!txtBuscar.getText().isEmpty()) {
            if (new Message().showConfirmation("Atencion!!", getStage(), "Seguro que desea eliminar el rol?")) {
                sr.elininarRol(rolDto.getId());
                limpiar();
            }
        } else {
            new Message().show(Alert.AlertType.INFORMATION, "", "Debe buscar un rol");
        }
    }

    @FXML
    private void atras(ActionEvent event) {
        limpiar();
        FlowController.getInstance().goView("Menu");
    }

    /**
     * Carga los puestos al combo box.
     */
    private void cargarPuestos() {
        puestos.addAll(sp.getPuestos());
        cbPuestos.setItems(puestos);
    }

    @FXML
    private void evtBuscar(ActionEvent event) {
         if (!txtBuscar.getText().isEmpty()) {
            RolesService serv = new RolesService();
            roles = FXCollections.observableArrayList(serv.buscarRol(Integer.parseInt(txtBuscar.getText())));
            tvRoles.setItems(roles);
            if(roles.size()<=0){
            new Message().show(Alert.AlertType.INFORMATION, "", "No existen roles para el numero buscado");
            }
        } else {
            new Message().show(Alert.AlertType.ERROR, "", "Debe ingresar el numero de rol a buscar");
        }
    }

    /**
     * Carga los formatos a los campos de texto.
     */
    public void formatos() {
        txtNameRol.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtNumeroRol.setTextFormatter(Formato.getInstance().integerFormat());
        txtBuscar.setTextFormatter(Formato.getInstance().integerFormat());
    }

    /**
     * limpia la pantalla
     */
    public void limpiar() {
        txtNameRol.clear();
        txtNumeroRol.clear();
        txtBuscar.clear();
        txtFechaSemana.clear();
        cbPuestos.setValue(null);
        rolDto = new RolDto();
        dias = FXCollections.observableArrayList();
        if(tvDias.getItems()!= null && tvRoles.getItems()!= null)
        {
            tvDias.getItems().clear();
            tvRoles.getItems().clear();
        }
        
    }

    @FXML
    private void evtLimpiar(ActionEvent event) {
        limpiar();
    }

    @FXML
    private void evtBuscado(MouseEvent event) {
            rolDto = tvRoles.getSelectionModel().getSelectedItem();
        ObservableList<DiaDto> listDias = FXCollections.observableArrayList();
        if (rolDto != null) {
            txtNameRol.setText(rolDto.getNombre());
            txtNumeroRol.setText(rolDto.getNumero().toString());
            txtFechaSemana.setText(rolDto.getFechaSemana());
            //cbRotativo.setSelected(rolDto.getFjo().equals("A"));
            cbPuestos.setValue(rolDto.getPuesto());
            System.out.println(rolDto.getPuesto());
            cbPuestos.getSelectionModel().select(pos(rolDto.getPuesto()));
            for (DiaDto listDia : rolDto.getDias()) {
                listDias.add(listDia);
            }
            dias = listDias;
            tvDias.setItems(dias);
        }
    }
    
        private int pos(PuestoDto p){
    int j=0;
        for (int i = 0; i < puestos.size(); i++) {
            if(puestos.get(i).getNombre().equals(p.getNombre())){
                j=i;
            }
        }
        return j;
    }
}
