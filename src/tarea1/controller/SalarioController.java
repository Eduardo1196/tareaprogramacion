/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.Observable;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import proyectoredes.controller.Controller;
import tarea1.model.DiaDto;
import tarea1.model.EmpleadoDto;
import tarea1.service.EmpleadoService;
import tarea1.util.FlowController;
import tarea1.util.Formato;
import tarea1.util.Message;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class SalarioController extends Controller implements Initializable {

    @FXML
    private JFXButton btnBuscar;
    @FXML
    private TableView<EmpleadoDto> tvEmpleados;
    @FXML
    private TableColumn<EmpleadoDto, String> tcNombre;
    @FXML
    private TableColumn<EmpleadoDto, String> tcPuesto;
    @FXML
    private JFXTextField txtEmpleado;
    @FXML
    private JFXTextField txtPrecio;
    @FXML
    private TableView<DiaDto> tvDias;
    @FXML
    private TableColumn<DiaDto, String> tcDia;
    @FXML
    private TableColumn<DiaDto, String> tcHoras;
    @FXML
    private JFXButton btnCalcular;
    @FXML
    private JFXButton btnAtras;
    @FXML
    private JFXTextField txtSalario;

    private EmpleadoService sEmp;
    private ObservableList<DiaDto> dias;
    private ObservableList<EmpleadoDto> empleados;
    @FXML
    private JFXButton btnLimpiar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        txtPrecio.setTextFormatter(Formato.getInstance().integerFormat());
        txtEmpleado.setTextFormatter(Formato.getInstance().letrasFormat(30));
        
        
        
        tcNombre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        tcPuesto.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getPuesto().getNombre()));

        tcDia.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        tcHoras.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorastrabajadas()));
    }

    @Override
    public void initialize() {
        dias = FXCollections.observableArrayList();
        empleados = FXCollections.observableArrayList();
        sEmp = new EmpleadoService();
    }

    @FXML
    private void evtBuscar(ActionEvent event) {
        dias.clear();
        empleados.clear();
        txtPrecio.clear();
        txtSalario.clear();
        empleados = FXCollections.observableList(sEmp.buscarEmpleado("%" + txtEmpleado.getText() + "%"));
//        tvEmpleados.setItems(empleados);
        tvEmpleados.setItems(remover(empleados));
    }

    @FXML
    private void evtTvEmpleados(MouseEvent event) {
        dias.clear();
        dias.addAll(tvEmpleados.getSelectionModel().getSelectedItem().getRol().getDias());
        tvDias.setItems(dias);
    }

    @FXML
    private void evtCalcular(ActionEvent event) {
        if (!txtPrecio.getText().isEmpty()) {
            txtSalario.setText("Salario: " + String.valueOf(dias.stream().mapToInt(
                    x -> Integer.parseInt(x.getHorastrabajadas())).sum() * Integer.parseInt(txtPrecio.getText())));
        } else {
            new Message().show(Alert.AlertType.ERROR, "Verificar datos", "Debe ingresar un precio por hora.");
        }
    }

    @FXML
    private void evtAtras(ActionEvent event) {
        limpiar();
        FlowController.getInstance().goView("Menu");
    }

    @FXML
    private void evtLimpiar(ActionEvent event) {
        limpiar();
    }

    private void limpiar() {
        dias.clear();
        empleados.clear();
        txtEmpleado.clear();
        txtPrecio.clear();
        txtSalario.clear();
        tvEmpleados.getItems().clear();
    }

    private ObservableList<EmpleadoDto> remover(ObservableList<EmpleadoDto> listO) {
        ObservableList<EmpleadoDto> list = FXCollections.observableArrayList(listO);
        for (EmpleadoDto emp : listO) {
            if (emp.getPuesto() == null) {
                list.remove(emp);
            }
        }
        return list;
    }
}
