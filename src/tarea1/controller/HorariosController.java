/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import proyectoredes.controller.Controller;
import tarea1.model.DiaDto;
import tarea1.model.EmpleadoDto;
import tarea1.model.PuestoDto;
import tarea1.model.RolDto;
import tarea1.service.EmpleadoService;
import tarea1.service.PuestoService;
import tarea1.util.AppContext;
import tarea1.util.Correo;
import tarea1.util.FlowController;
import tarea1.util.Message;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class HorariosController extends Controller implements Initializable {

    @FXML
    private TableView<EmpleadoDto> tblEmpleados;
    @FXML
    private JFXComboBox<String> cbVerPor;
    @FXML
    private TableView<PuestoDto> tblPuestos;
    @FXML
    private TableColumn<PuestoDto, String> tcPuesPuesto;
    @FXML
    private JFXTextField tfNombre;
    @FXML
    private Label txtRolesAsig;
    @FXML
    private Label txtHorasTrab;
    @FXML
    private JFXButton btnExportar;
    @FXML
    private TableColumn<EmpleadoDto, String> tcEmpNombre;
    @FXML
    private TableColumn<EmpleadoDto, String> tcEmpLunes;
    @FXML
    private TableColumn<EmpleadoDto, String> tcEmpMartes;
    @FXML
    private TableColumn<EmpleadoDto, String> tcEmpMiercoles;
    @FXML
    private TableColumn<EmpleadoDto, String> tcEmpJueves;
    @FXML
    private TableColumn<EmpleadoDto, String> tcEmpViernes;
    @FXML
    private TableColumn<EmpleadoDto, String> tcEmpSabado;
    @FXML
    private TableColumn<EmpleadoDto, String> tcEmpDomingo;
    @FXML
    private TableColumn<PuestoDto, String> tcPuesLunes;
    @FXML
    private TableColumn<PuestoDto, String> tcPuesMartes;
    @FXML
    private TableColumn<PuestoDto, String> tcPuesMiercoles;
    @FXML
    private TableColumn<PuestoDto, String> tcPuesJueves;
    @FXML
    private TableColumn<PuestoDto, String> tcPuesViernes;
    @FXML
    private TableColumn<PuestoDto, String> tcPuesSabado;
    @FXML
    private TableColumn<PuestoDto, String> tcPuesDomiingo;
    
    
    ObservableList<EmpleadoDto> empleados;
    ObservableList<PuestoDto> puestos;
    EmpleadoService empS;
    PuestoService ps;
    HSSFWorkbook hssfwbook;
    HSSFSheet hssfsheet;
    HSSFRow hssfrow,hssfrowH,hssfrowC;
    CellStyle cellstyle , style;
    HSSFFont font;
    @FXML
    private Label txtNombreRol;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ObservableList<String> verPor = FXCollections.observableArrayList();
        empleados = FXCollections.observableArrayList();
        puestos = FXCollections.observableArrayList();
        empS = new EmpleadoService();
        ps = new PuestoService();
        
        verPor.add("Puesto");
        verPor.add("Empleado");
        cbVerPor.setItems(verPor);
        
        tcEmpNombre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        //Perdon profe no tuve opcion u.u 
        tcEmpLunes.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Lunes")).findFirst().get().getHorainicio() + "-" +
                x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Lunes")).findFirst().get().getHorafin()));
        tcEmpMartes.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Martes")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Martes")).findFirst().get().getHorafin()));
        tcEmpMiercoles.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Miercoles")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Miercoles")).findFirst().get().getHorafin()));
        tcEmpJueves.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Jueves")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Jueves")).findFirst().get().getHorafin()));
        tcEmpViernes.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Viernes")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Viernes")).findFirst().get().getHorafin()));
        tcEmpSabado.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Sabado")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Sabado")).findFirst().get().getHorafin()));
        tcEmpDomingo.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Domingo")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRol().getDias().stream()
                .filter(l -> l.getNombre().equals("Domingo")).findFirst().get().getHorafin()));
        
        
        tcPuesPuesto.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        
        tcPuesLunes.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Lunes")).findFirst().get().getHorainicio() + "-" +
                x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Lunes")).findFirst().get().getHorafin()));
        tcPuesMartes.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Martes")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Martes")).findFirst().get().getHorafin()));
        tcPuesMiercoles.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Miercoles")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Miercoles")).findFirst().get().getHorafin()));
        tcPuesJueves.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Jueves")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Jueves")).findFirst().get().getHorafin()));
        tcPuesViernes.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Viernes")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Viernes")).findFirst().get().getHorafin()));
        tcPuesSabado.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Sabado")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Sabado")).findFirst().get().getHorafin()));
        tcPuesDomiingo.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Domingo")).findFirst().get().getHorainicio() + "-" + 
                x.getValue().getRoles().get(0).getDias().stream()
                .filter(l -> l.getNombre().equalsIgnoreCase("Domingo")).findFirst().get().getHorafin()));
        
        
    }    

    @FXML
    private void evtCbVerPor(ActionEvent event) {
        if(cbVerPor.getSelectionModel().getSelectedItem() != null){
            txtHorasTrab.setText("0");
            txtNombreRol.setText("");
            if(cbVerPor.getSelectionModel().getSelectedItem().equals("Puesto")){
                tblEmpleados.setDisable(true);
                tblEmpleados.setVisible(false);
                
                tblPuestos.setDisable(false);
                tblPuestos.setVisible(true);
            }else
            if(cbVerPor.getSelectionModel().getSelectedItem().equals("Empleado")){
                tblEmpleados.setDisable(false);
                tblEmpleados.setVisible(true);
                
                tblPuestos.setDisable(true);
                tblPuestos.setVisible(false);
            }   
        }
    }

    @FXML
    private void btnBuscar(ActionEvent event) {
        txtNombreRol.setText("");
        if(cbVerPor.getSelectionModel().getSelectedItem() != null && !tfNombre.getText().isEmpty()){
            btnExportar.setVisible(true);
            tblEmpleados.getItems().clear();
            tblPuestos.getItems().clear();
            if(cbVerPor.getSelectionModel().getSelectedItem().equals("Empleado")){
                for(EmpleadoDto emp: empS.buscarEmpleado(tfNombre.getText())){
                    if(emp.getRol() != null){
                        empleados.add(emp);
                        txtHorasTrab.setText(""+empS.horasTrabajadas(emp.getRol()));
                    }else{
                        new Message().show(Alert.AlertType.INFORMATION, "", "El empleado no tiene roles asignados");
                    }
                }
                if(empleados != null){
                    tblEmpleados.setItems(empleados);
                }else
                    new Message().show(Alert.AlertType.INFORMATION, "", "No hay resultados de la busqueda");
            }else
            if(cbVerPor.getSelectionModel().getSelectedItem().equals("Puesto")){
                List<RolDto> roles = new ArrayList<>();
                for(PuestoDto p: ps.BuscarPuesto(tfNombre.getText())){
                    if(p.getRoles() != null){
                        for(RolDto r: p.getRoles()){
                            puestos.add(new PuestoDto(p,r));
                            roles.add(r);
                        }
                    }else{
                        new Message().show(Alert.AlertType.INFORMATION, "", "El puesto no tiene roles asignados");
                    }
                }
                txtHorasTrab.setText(""+empS.horasTrabajadasList(roles));
                tblPuestos.setItems(puestos);
                
            }
        }else{
            if(tfNombre.getText().isEmpty()){
                new Message().show(Alert.AlertType.INFORMATION, "", "Debe escribir un criterio de busqueda");
            }
            if(cbVerPor.getSelectionModel().getSelectedItem() == null){
                new Message().show(Alert.AlertType.INFORMATION, "", "Debe seleccionar el tipo de busqueda");
            }
        }
    }

    @FXML
    private void btnAtras(ActionEvent event) {
        FlowController.getInstance().goView("Menu");
        limpiar();
    }

    @FXML
    private void btnLimpiar(ActionEvent event) {
        limpiar();
    }
    /**
     * Limpia todo
     */
    private void limpiar(){
        tblEmpleados.getItems().clear();
        tblPuestos.getItems().clear();
        
        tfNombre.clear();
        cbVerPor.getSelectionModel().clearSelection();
        
        txtHorasTrab.setText("0");
        txtNombreRol.setText("");
        txtRolesAsig.setText(""+empS.cantEmpleadosRol());
    }

    @Override
    public void initialize() {
        btnExportar.setVisible(false);
        txtRolesAsig.setText(""+empS.cantEmpleadosRol());
    }

    @FXML
    private void evtTblEmpleados(MouseEvent event) {
        if(tblEmpleados.getSelectionModel().getSelectedItem() != null){
            txtNombreRol.setText("Rol: "+ tblEmpleados.getSelectionModel().getSelectedItem().getRol().getNombre());
        }
    }
    
    @FXML
    private void evtTblPuestos(MouseEvent event) {
        if(tblPuestos.getSelectionModel().getSelectedItem()!= null){
            txtNombreRol.setText("Rol: "+tblPuestos.getSelectionModel().getSelectedItem().getRoles().get(0).getNombre());
        }
    }


    @FXML
    private void evtVerTodos(ActionEvent event) {
        txtNombreRol.setText("");
        if(cbVerPor.getSelectionModel().getSelectedItem() != null){
            tblEmpleados.getItems().clear();
            tblPuestos.getItems().clear();
            if(cbVerPor.getSelectionModel().getSelectedItem().equals("Puesto")){
                List<RolDto> roles = new ArrayList<>();
                for(PuestoDto p: ps.getPuestos()){
                    if(p.getRoles() != null){
                        for(RolDto r: p.getRoles()){
                            puestos.add(new PuestoDto(p,r));
                            roles.add(r);
                        }
                    }
                }
                txtHorasTrab.setText(""+empS.horasTrabajadasList(roles));
                tblPuestos.setItems(puestos);
            }else{
                if(cbVerPor.getSelectionModel().getSelectedItem().equals("Empleado")){
                    List<RolDto> roles = new ArrayList<>();
                    for(EmpleadoDto emp: empS.getEmpleados()){
                        if(emp.getRol() != null){
                            empleados.add(emp);
                            roles.add(emp.getRol());
                        }
                    }
                    if(!empleados.isEmpty()){
                        txtHorasTrab.setText(""+empS.horasTrabajadasList(roles));
                        tblEmpleados.setItems(empleados);
                }else
                    new Message().show(Alert.AlertType.INFORMATION, "", "No hay resultados de la busqueda");
                }
            }
        }else{
            new Message().show(Alert.AlertType.WARNING, "", "Debe seleccinar el criterio de busqueda");
        }
        btnExportar.setVisible(true);
    }

    @FXML
    private void evtExportar(ActionEvent event) throws IOException
    {
        exportar();
    }
    public void exportar() throws IOException
    {
        hssfwbook = new HSSFWorkbook();
	hssfsheet = hssfwbook.createSheet();
	hssfwbook.setSheetName(0,"Reporte de Consultas");
        cellstyle = hssfwbook.createCellStyle();
	font = hssfwbook.createFont();
	font.setBold(true);
	cellstyle.setFont(font);
	style = hssfwbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

	File file = new File("Reporte.xls"); File file2 = new File("Reportes.xls");
        
	String[] enc =new String[]
        {
	  "Puesto" ,"Lunes" , "Martes" , "Miercoles" , "Jueves" , "Viernes" ,"Sabado", "Domingo"
	};
        String[] emp =new String[]
        {
	  "Nombre" ,"Lunes" , "Martes" , "Miercoles" , "Jueves" , "Viernes" ,"Sabado", "Domingo"
	};
	if(tblPuestos.isVisible() && tblPuestos.getItems()!= null)
        {
            hssfrow = hssfsheet.createRow(0);
            for (int i = 0; i < enc.length; ++i) 
            {
                String en = enc[i];
                HSSFCell cell = hssfrow.createCell(i);
                cell.setCellStyle(cellstyle);
                cell.setCellValue(en);
                cell.setCellStyle(style);
            }
            for(int i = 0; i <puestos.size(); i++)
            {
                hssfrow = hssfsheet.createRow(i+1);
                for(int j = 0; j < puestos.size()-(puestos.size()-1); j++)
                {   
                    
                    hssfrow.createCell(0).setCellValue(tcPuesPuesto.getCellData(i));
                    hssfrow.createCell(j+1).setCellValue(tcPuesLunes.getCellData(i));
                    hssfrow.createCell(j+2).setCellValue(tcPuesMartes.getCellData(i));
                    hssfrow.createCell(j+3).setCellValue(tcPuesMiercoles.getCellData(i));
                    hssfrow.createCell(j+4).setCellValue(tcPuesJueves.getCellData(i));
                    hssfrow.createCell(j+5).setCellValue(tcPuesViernes.getCellData(i));
                    hssfrow.createCell(j+6).setCellValue(tcPuesSabado.getCellData(i));
                    hssfrow.createCell(j+7).setCellValue(tcPuesDomiingo.getCellData(i));
               }
            }
            hssfrowH= hssfsheet.createRow(puestos.size()+3);
            HSSFCell cell = hssfrowH.createCell(0);
            cell.setCellValue("Horas Trab.");
            cell.setCellStyle(style);
            HSSFCell cellH=hssfrowH.createCell(1);
            cellH.setCellValue(txtHorasTrab.getText());
            
            
            hssfrowC= hssfsheet.createRow(puestos.size()+4);
            HSSFCell cell1 = hssfrowC.createCell(0);
            cell1.setCellValue("Roles Asig.");
            cell1.setCellStyle(style);
            HSSFCell cellC= hssfrowC.createCell(1);
            cellC.setCellValue(txtRolesAsig.getText());
            hssfwbook.write(file);
            Desktop.getDesktop().open(file);
        }
        else 
        {
            if(tblEmpleados.isVisible() && tblEmpleados.getItems() != null)
            {
            hssfrow = hssfsheet.createRow(0);
            for (int i = 0; i < enc.length; ++i) 
            {
                String em = emp[i];
                HSSFCell cell = hssfrow.createCell(i);
                cell.setCellStyle(cellstyle);
                cell.setCellValue(em);
                cell.setCellStyle(style);
            }
            for(int i = 0; i <empleados.size(); i++)
            {
                hssfrow = hssfsheet.createRow(i+1);
                for(int j = 0; j <empleados.size()-(empleados.size()-1); j++)       //fila
                {  
                    hssfrow.createCell(0).setCellValue(tcEmpNombre.getCellData(i));
                    hssfrow.createCell(j+1).setCellValue(tcEmpLunes.getCellData(i));
                    hssfrow.createCell(j+2).setCellValue(tcEmpMartes.getCellData(i));
                    hssfrow.createCell(j+3).setCellValue(tcEmpMiercoles.getCellData(i));
                    hssfrow.createCell(j+4).setCellValue(tcEmpJueves.getCellData(i));
                    hssfrow.createCell(j+5).setCellValue(tcEmpViernes.getCellData(i));
                    hssfrow.createCell(j+6).setCellValue(tcEmpSabado.getCellData(i));
                    hssfrow.createCell(j+7).setCellValue(tcEmpDomingo.getCellData(i));
                    
                }
            }
            hssfrowH= hssfsheet.createRow(empleados.size()+3);
            HSSFCell cell = hssfrowH.createCell(0);
            cell.setCellValue("Horas Trab.");
            cell.setCellStyle(style);
            HSSFCell cellH=hssfrowH.createCell(1);
            cellH.setCellValue(txtHorasTrab.getText());
            
            
            hssfrowC= hssfsheet.createRow( empleados.size()+4);
            HSSFCell cell1 = hssfrowC.createCell(0);
            cell1.setCellValue("Roles Asig.");
            cell1.setCellStyle(style);
            HSSFCell cellC= hssfrowC.createCell(1);
            cellC.setCellValue(txtRolesAsig.getText());
            
            
            hssfwbook.write(file);
            hssfwbook.write(file2);
            Desktop.getDesktop().open(file);
            if(tblEmpleados.getSelectionModel().getSelectedItem() != null)
            {
               new Correo().Enviar(tblEmpleados.getSelectionModel().getSelectedItem().getCorreo(), "Reporte de consultas", file2.getAbsolutePath(),file2.getName()); 
            }
            else
            {
                new Message().show(Alert.AlertType.INFORMATION,"","Debe seleccionar un empleado para enviar las consultas al correo");
            }
           
          }

        }   
    
    }
}
