/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import java.net.URL;
import java.time.LocalTime;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import proyectoredes.controller.Controller;
import tarea1.model.DiaDto;
import tarea1.util.AppContext;
import tarea1.util.FlowController;
import tarea1.util.Formato;
import tarea1.util.Message;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class MantDiasController extends Controller implements Initializable {

    @FXML
    private JFXCheckBox cxbLibre;
    @FXML
    private JFXTimePicker tpInicio;
    @FXML
    private JFXTimePicker tpFin;
    @FXML
    private JFXTextField txtHrsTrabajadas;
    @FXML
    private TableView<DiaDto> tvDias;
    @FXML
    private TableColumn<DiaDto, String> tcNombre;
    @FXML
    private TableColumn<DiaDto, String> tcLibre;
    @FXML
    private TableColumn<DiaDto, String> tcHoraInicio;
    @FXML
    private TableColumn<DiaDto, String> tcHoraSalida;
    @FXML
    private TableColumn<DiaDto, String> tcHorasTrabajadas;
    @FXML
    private JFXButton btnAceptar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnActualizar;

    DiaDto diaBase;
    ObservableList<DiaDto> dias;
    ObservableList<String> sem;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        sem = FXCollections.observableArrayList();
        sem.addAll("Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo");

        txtHrsTrabajadas.setTextFormatter(Formato.getInstance().integerFormat());

        tcNombre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre()));
        tcLibre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getLibre()));
        tcHoraInicio.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorainicio()));
        tcHoraSalida.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorafin()));
        tcHorasTrabajadas.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getHorastrabajadas()));

    }

    @Override
    public void initialize() {
        dias = FXCollections.observableArrayList();
        generarDias();

    }

    @FXML
    private void aceptar(ActionEvent event) {
        AppContext.getInstance().set("Semana", dias);
        FlowController.getInstance().goView("MantRoles");
    }

    @FXML
    private void cancelar(ActionEvent event) {
        if (new Message().showConfirmation("", getStage(), "Desea descartar los cambios?")) {
            dias.clear();
            FlowController.getInstance().goView("MantRoles");
        }
    }

    @FXML
    private void actualizar(ActionEvent event) {
        actualizarDia(diaBase);
        tvDias.setItems(dias);

    }

    private void generarDias() {
        dias.clear();
        if (!(Boolean) AppContext.getInstance().get("Existe")) {
            for (String dia : sem) {
                DiaDto d = new DiaDto();
                d.setNombre(dia);
                d.setHorafin("16:00");
                d.setHorainicio("07:00");
                d.setHorastrabajadas("9");
                d.setLibre("N");
                this.dias.add(d);
            }
        } else {
            dias.addAll((ObservableList<DiaDto>) AppContext.getInstance().get("SemanaRol"));
        }
        tvDias.setItems(dias);

    }

    public void cargarDia(DiaDto dia) {
        if (dia != null) {
            txtHrsTrabajadas.setText(dia.getHorastrabajadas());
            cxbLibre.setSelected((dia.getLibre().equals("S")) ? true : false);
            tpInicio.setValue(LocalTime.parse(dia.getHorainicio()));
            tpFin.setValue(LocalTime.parse(dia.getHorafin()));
        }
    }

    public void actualizarDia(DiaDto d) {
        ObservableList<DiaDto> list = FXCollections.observableArrayList();
        list.clear();
        list.setAll(dias);
        for (DiaDto diaDto : list) {
            if (diaDto.equals(d)) {
                if (cxbLibre.isSelected()) {
                    d.setHorafin("00:00");
                    d.setHorainicio("00:00");
                    d.setHorastrabajadas("0");
                    d.setLibre("S");
                } else {
                    diaDto.setHorafin(tpFin.getValue().toString());
                    diaDto.setHorainicio(tpInicio.getValue().toString());
                    diaDto.setHorastrabajadas(txtHrsTrabajadas.getText());
                    diaDto.setLibre(cxbLibre.isSelected() ? "S" : "N");
                }
            }
        }
        dias.clear();
        dias.addAll(list);
        limpiar();
    }

    @FXML
    private void etvTablaDias(MouseEvent event) {
        diaBase = tvDias.getSelectionModel().getSelectedItem();
        cargarDia(diaBase);
        habilitar();
    }

    /**
     * Limpia los componetes
     */
    public void limpiar() {
        tpFin.setValue(null);
        tpInicio.setValue(null);
        txtHrsTrabajadas.clear();
        cxbLibre.setSelected(false);
    }

    public void habilitar() {
        tpFin.setDisable(cxbLibre.isSelected());
        tpInicio.setDisable(cxbLibre.isSelected());
        txtHrsTrabajadas.setDisable(cxbLibre.isSelected());

    }

    @FXML
    private void evtHabiLibre(ActionEvent event) {
        habilitar();
    }
}
