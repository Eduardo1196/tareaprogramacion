/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import proyectoredes.controller.Controller;
import tarea1.model.PuestoDto;
import tarea1.service.PuestoService;
import tarea1.util.AppContext;
import tarea1.util.FlowController;
import tarea1.util.Message;

/**
 * FXML Controller class
 *
 * @author Arlick
 */
public class MantPuestosController extends Controller implements Initializable {

    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXTextField txt_nombre;
    @FXML
    private JFXTextField txt_codigo;
    @FXML
    private JFXTextArea txt_descripcion;
    @FXML
    private TableView<PuestoDto> tbPuesto;
    @FXML
    private TableColumn<PuestoDto, String> tcNombre;
    @FXML
    private TableColumn<PuestoDto, String> tcCodigo;
    @FXML
    private JFXTextField txt_nombre1;
    
    PuestoDto puesto;
    PuestoService ps;
    ObservableList <PuestoDto> puestos;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ps = new PuestoService();
        puesto = new PuestoDto();
        
        tcNombre.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getNombre())); 
        tcCodigo.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getCodigo()));
        
        puestos = FXCollections.observableArrayList();
        
        AppContext.getInstance().set("PuestoService", ps);
        
        btnGuardar.setTooltip(new Tooltip("Guardar"));
    }    

    @FXML
    private void evtAtras(ActionEvent event) 
    {
        FlowController.getInstance().goView("Menu");
        Limpiar();
    }

    @FXML
    private void evtGuardar(ActionEvent event) {
        if(txt_codigo.getText().isEmpty() || txt_descripcion.getText().isEmpty() || txt_nombre1.getText().isEmpty())
        {
            new Message().show(Alert.AlertType.ERROR,"Error al guardar","Debe completar todos los espacios para guardar el puesto");
        }
        else
        { 
            puesto.setCodigo(txt_codigo.getText());
            puesto.setDescripcion(txt_descripcion.getText());
            puesto.setNombre(txt_nombre1.getText());
            ps.InsertarPuesto(puesto);
            Limpiar();
        }
    }

    @Override
    public void initialize() {
      
    }

    @FXML
    private void evtLimpiar(ActionEvent event) 
    {
        Limpiar();
    }

    @FXML
    private void evtBuscarPuesto(ActionEvent event) 
    {
        if(!txt_nombre.getText().isEmpty()){
            puestos = (FXCollections.observableArrayList(ps.BuscarPuesto(txt_nombre.getText())));
            tbPuesto.setItems(puestos);
            
        }else{
            new Message().show(Alert.AlertType.ERROR,"Error al buscar","Debe ingresar el nombre para buscar el puesto");
        }
    }

    @FXML
    private void evtTblPuesto(MouseEvent event) {
        if(tbPuesto.getSelectionModel().getSelectedItem()!=null){
            puesto = tbPuesto.getSelectionModel().getSelectedItem();
            txt_nombre1.setText(puesto.getNombre());
            txt_descripcion.setText(puesto.getDescripcion());
            txt_codigo.setText(puesto.getCodigo());
        }
    }

    @FXML
    private void evtEliminar(ActionEvent event) 
    {
        if(!txt_nombre1.getText().isEmpty())
        {
            ps.EliminarPuesto(puesto.getId());
            new Message().show(Alert.AlertType.INFORMATION,"Eliminar puesto","El puesto se ha eliminado correctamente");
            Limpiar();
        }
        else
        {
            new Message().show(Alert.AlertType.ERROR,"Error al eliminar","Debe ingresar el nombre del puesto para eliminar ");
        }
    }
    public void Limpiar()
    {
        txt_nombre.clear();
        txt_descripcion.clear();
        txt_codigo.clear();
        txt_nombre1.clear();
        tbPuesto.getItems().clear();
    }
}
