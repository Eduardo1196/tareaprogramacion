/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.service;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Alert;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import tarea1.model.DiaDto;
import tarea1.model.Empleado;
import tarea1.model.EmpleadoDto;
import tarea1.model.Puesto;
import tarea1.model.PuestoDto;
import tarea1.model.Rol;
import tarea1.model.RolDto;
import tarea1.util.AppContext;
import tarea1.util.ControlBD;
import tarea1.util.Message;

public class EmpleadoService {

    ControlBD ctrl;

    public EmpleadoService() {
        ctrl = (ControlBD) AppContext.getInstance().get("ControlBD");
    }

    public void GuardarEmpleado(EmpleadoDto empDto) {
        Empleado emp = new Empleado(empDto);
        try {
            if (emp.getEmpId() == null) {
                ctrl.getEt().begin();
                ctrl.getEm().persist(emp);
                ctrl.getEt().commit();
                new Message().show(Alert.AlertType.CONFIRMATION, "Guardado", "El Empleado se ha guardado con exito!");
            } else {
                ctrl.getEt().begin();
                ctrl.getEm().merge(emp);
                ctrl.getEt().commit();
                new Message().show(Alert.AlertType.CONFIRMATION, "Actualizado", "el empleado se actualizo correctamente");
            }
        } catch (Exception ex) {
            new Message().show(Alert.AlertType.ERROR, "Guardar empleado", "Se presento un error al guardar el empleado");
        }
    }

    public List<EmpleadoDto> buscarEmpleado(String nombre) {
        List<EmpleadoDto> empDto = new ArrayList();
        List<Empleado> emp;
        try {
            Query query = ctrl.getEm().createNamedQuery("Empleado.findByEmpNombre", Empleado.class);
            query.setParameter("empNombre", "%" + nombre + "%");
            emp = query.getResultList();
            for (Empleado e : emp) {
                empDto.add(new EmpleadoDto(e));
            }
            return empDto;
        } catch (NoResultException ex) {
            new Message().show(Alert.AlertType.ERROR, "", "No existen Empleados en el registro");
        } catch (Exception ex) {
            System.out.println("Excepcion " + ex);
        }
        return null;
    }

    public void EliminarEmpleado(Long id) {
        Empleado emp = new Empleado();
        try {
            Query query = ctrl.getEm().createNamedQuery("Empleado.findByEmpId", Empleado.class);
            query.setParameter("empId", id);
            emp = (Empleado) query.getSingleResult();
            ctrl.getEt().begin();
            ctrl.getEm().remove(emp);
            ctrl.getEt().commit();

        } catch (Exception ex) {
            new Message().show(Alert.AlertType.ERROR, "Error", "Error al Eliminar Empleado");
        }
    }

    public void asignarRol(EmpleadoDto empleadoDto, PuestoDto puestoDto, RolDto rolDto) {
        Empleado emp = new Empleado(empleadoDto);
        Puesto pt = new Puesto();
        Rol rol = new Rol();
        try {
            Query query = ctrl.getEm().createNamedQuery("Empleado.findByEmpId", Empleado.class);
            query.setParameter("empId", emp.getEmpId());
            emp = (Empleado) query.getSingleResult();

            Query query1 = ctrl.getEm().createNamedQuery("Puesto.findByPuesId", Puesto.class);
            query1.setParameter("puesId", puestoDto.getId());
            pt = (Puesto) query1.getSingleResult();

            Query query2 = ctrl.getEm().createNamedQuery("Rol.findByRolId", Rol.class);
            query2.setParameter("rolId", rolDto.getId());
            rol = (Rol) query2.getSingleResult();

            emp.setEmpPuesId(pt);
            emp.setEmpRolId(rol);

            ctrl.getEt().begin();
            ctrl.getEm().persist(emp);
            ctrl.getEt().commit();
            new Message().show(Alert.AlertType.CONFIRMATION, "Asignar Rol", "El rol se asigno correctamente");

        } catch (Exception ex) {
            new Message().show(Alert.AlertType.ERROR, "Error al asignar Rol", ex.getMessage());
        }
    }

    public List<EmpleadoDto> getEmpleados() {
        List<EmpleadoDto> empleadoDto = new ArrayList();
        List<Empleado> empleados;
        try {
            Query query = ctrl.getEm().createNamedQuery("Empleado.findAll", Empleado.class);
            empleados = query.getResultList();
            for (Empleado e : empleados) {
                empleadoDto.add(new EmpleadoDto(e));
            }
            return empleadoDto;
        } catch (NoResultException ex) {
            new Message().show(Alert.AlertType.ERROR, "", "No existen Empleados en el registro");
        } catch (Exception ex) {
            new Message().show(Alert.AlertType.ERROR, "", ex.getMessage());
        }
        return null;
    }

    public int cantEmpleadosRol() {
        List<EmpleadoDto> empleadoDto = new ArrayList();
        empleadoDto = getEmpleados();

        int cantRol = 0;
        cantRol = (int) empleadoDto.stream().filter(x -> x.getRol() != null).count();
        System.out.println("rolesAsig " + cantRol);

        return cantRol;
    }

    public int horasTrabajadas(RolDto rol) {
        List<DiaDto> dias = new ArrayList<DiaDto>();
        dias = rol.getDias();
        int ht = 0;
        ht = (int) dias.stream().mapToInt(x -> Integer.parseInt(x.getHorastrabajadas())).sum();

        return ht;
    }

    public int horasTrabajadasList(List<RolDto> roles) {
        List<DiaDto> dias = new ArrayList<DiaDto>();
        for (RolDto rol : roles) {
            dias.addAll(rol.getDias());
        }
        int ht = 0;
        ht = (int) dias.stream().mapToInt(x -> Integer.parseInt(x.getHorastrabajadas())).sum();

        return ht;
    }
}
