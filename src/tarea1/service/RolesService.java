/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javafx.scene.control.Alert;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import tarea1.model.Dia;
import tarea1.model.DiaDto;
import tarea1.model.Empleado;
import tarea1.model.EmpleadoDto;
import tarea1.model.Puesto;
import tarea1.model.PuestoDto;
import tarea1.model.Rol;
import tarea1.model.RolDto;
import tarea1.util.AppContext;
import tarea1.util.ControlBD;
import tarea1.util.Message;

/**
 *
 * @author liedu
 */
public class RolesService {

    ControlBD ctrl;

    public RolesService() {
        ctrl = (ControlBD) AppContext.getInstance().get("ControlBD");
    }

    public void guardarRoles(RolDto rolDto, PuestoDto puestoDto) {
        Rol rol = new Rol(rolDto);
        Puesto puesto;
        puesto = ctrl.getEm().find(Puesto.class, puestoDto.getId());
        rol.setRolPuesId(puesto);
        try {
            if (rol.getRolId() == null) {
                ctrl.getEt().begin();
                ctrl.getEm().persist(rol);
                ctrl.getEm().flush();
                ctrl.getEm().refresh(puesto);
                ctrl.getEt().commit();

                new Message().show(Alert.AlertType.CONFIRMATION, "Guardado", "El Rol se ha guardado con exito!");
            } else {
                ctrl.getEt().begin();
                ctrl.getEm().merge(rol);
                ctrl.getEm().flush();
                ctrl.getEm().refresh(puesto);
                ctrl.getEt().commit();
                new Message().show(Alert.AlertType.CONFIRMATION, "Atualizar", "El Rol se ha actualizo con exito!");
            }
            guardarDias(rol, rolDto.getDias());
        } catch (Exception e) {
            System.out.println("Eror al guardar rol" + e.getMessage());
        }
    }
    
    public void guardarRol(RolDto rolDto, PuestoDto puestoDto, Long id) {
        Rol rol = ctrl.getEm().find(Rol.class, rolDto.getId());
        rol.actualizarRol(rolDto);
        Puesto puesto;
        puesto = ctrl.getEm().find(Puesto.class, puestoDto.getId());
        Empleado emp = ctrl.getEm().find(Empleado.class, id);
        rol.setRolPuesId(puesto);
        emp.setEmpRolId(rol);
        try {
            ctrl.getEt().begin();
            ctrl.getEm().persist(rol);
            ctrl.getEm().flush();
            ctrl.getEm().refresh(emp);
            ctrl.getEt().commit();
        } catch (Exception e) {
            System.out.println("Rol rotativo" + e.getMessage());
        }
    }

    public void guardarDias(Rol rol, List<DiaDto> ldia) {
        List<DiaDto> dias = ldia;
        Dia dia;
        try {
            for (DiaDto dia1 : dias) {
                dia = new Dia(dia1);
                dia.setDiaRolId(rol);
                if (dia.getDiaId() == null) {
                    ctrl.getEt().begin();
                    ctrl.getEm().persist(dia);
                    ctrl.getEm().flush();
                    ctrl.getEm().refresh(rol);
                    ctrl.getEt().commit();
                } else {
                    ctrl.getEt().begin();
                    ctrl.getEm().merge(dia);
                    ctrl.getEm().flush();
                    ctrl.getEm().refresh(rol);
                    ctrl.getEt().commit();
                }

            }
        } catch (Exception e) {
        }
    }

    public void elininarRol(Long id) {
        Rol rol = new Rol();
        try {
            Query query = ctrl.getEm().createNamedQuery("Rol.findByRolId", Rol.class);
            query.setParameter("rolId", id);
            rol = (Rol) query.getSingleResult();
            ctrl.getEt().begin();
            ctrl.getEm().remove(rol);
            ctrl.getEt().commit();

        } catch (NoResultException ex) {
            new Message().show(Alert.AlertType.ERROR, "", "No existe un rol a eliminar");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                new Message().show(Alert.AlertType.ERROR, "Error", "No se puede eliminar el cliente porque tiene relaciones con otros registros.");
            } else {
                new Message().show(Alert.AlertType.ERROR, "Error", "Error al eliminar el rol");
            }
        }
    }

    public List<RolDto> buscarRol(Integer code) {
        List<RolDto> rol = new ArrayList<>();
        List<Rol> eRol;
        RolDto roldto;
        try {
            Query query = ctrl.getEm().createNamedQuery("Rol.findByRolNumero", Rol.class);
            query.setParameter("rolNumero", code);
            eRol = (List<Rol>) query.getResultList();
            for (Rol r : eRol) {
                roldto = new RolDto(r);
                roldto.setPuesto(new PuestoDto(r.getRolPuesId()));
                rol.add(roldto);
            }
            return rol;
        } catch (NoResultException ex) {
            new Message().show(Alert.AlertType.ERROR, "Busacr rol", "No exixte un rol con el numero indicado");
            return null;
        } catch (Exception e) {
            new Message().show(Alert.AlertType.ERROR, "Buscar rol", "Error al consultar" + e.getMessage());
            return null;
        }
    }

    public List<RolDto> getRoles() {
        List<RolDto> roles = new ArrayList<>();
        List<Rol> eroles = new ArrayList<>();
        try {
            Query query = ctrl.getEm().createNamedQuery("Rol.findAll", Rol.class);
            eroles = (List<Rol>) query.getResultList();
            for (Rol rol : eroles) {
                roles.add(new RolDto(rol));
            }
            System.out.println(eroles.stream().mapToInt(x -> x.getRolNumero()).max());
            return roles;
        } catch (NoResultException ex) {
            new Message().show(Alert.AlertType.ERROR, "", "No existen roles en el registro");
            return null;
        } catch (Exception e) {
            new Message().show(Alert.AlertType.ERROR, "Error al consultar", e.getMessage());
            return null;
        }
    }

    public RolDto setRotativo(RolDto rolDto) {
        Rol rol = new Rol(rolDto);
        try {
            Query query = ctrl.getEm().createNamedQuery("Rol.findByRolId", Rol.class);
            query.setParameter("rolId", rolDto.getId());
            rol = (Rol) query.getSingleResult();

            rol.setRolFijo("A");

            ctrl.getEt().begin();
            ctrl.getEm().merge(rol);
            ctrl.getEt().commit();

            rolDto = new RolDto(rol);
            return rolDto;
        } catch (Exception ex) {
            return rolDto;
        }
    }

    public void verificarRotativos() {
        EmpleadoService em = new EmpleadoService();
        List<EmpleadoDto> emps = new ArrayList<>();
        emps = em.getEmpleados();
        RolDto rol = null;

        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setMinimalDaysInFirstWeek(4);
        calendar.setTime(calendar.getTime());
        int ns = calendar.get(Calendar.WEEK_OF_YEAR);

        for (EmpleadoDto e : emps) {
            if (e.getRol() != null && Integer.parseInt(e.getRol().getFechaSemana()) < ns) {
                if (e.getRol().getFjo().equals("A")) {
                    if (e.getPuesto().getRoles().stream().anyMatch(x -> x.getNumero() == e.getRol().getNumero() + 1)) {
                        rol = e.getPuesto().getRoles().stream().filter(x -> x.getNumero() == e.getRol().getNumero() + 1).findFirst().get();
                    }
                    if (rol != null) {
                        rol = new RolDto(ctrl.getEm().find(Rol.class, rol.getId()));
                        rol.setFechaSemana(String.valueOf(ns));
                        guardarRol(rol, e.getPuesto(), e.getId());
                    } else {
                        //volver a inicio
                        rol = e.getPuesto().getRoles().get(0);
                        rol.setFechaSemana(String.valueOf(ns));
                        guardarRol(rol, e.getPuesto(), e.getId());
                    }
                }
            }
        }
    }
}
