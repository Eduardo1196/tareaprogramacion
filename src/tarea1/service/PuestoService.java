/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javafx.scene.control.Alert;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import tarea1.model.Puesto;
import tarea1.model.PuestoDto;
import tarea1.model.Rol;
import tarea1.model.RolDto;
import tarea1.util.AppContext;
import tarea1.util.ControlBD;
import tarea1.util.Message;

/**
 *
 * @author Susana
 */
public class PuestoService {

    private ControlBD cbd;
    private RolesService rs;

    public PuestoService() {
        cbd = (ControlBD) AppContext.getInstance().get("ControlBD");
        rs = new RolesService();
    }

    public void InsertarPuesto(PuestoDto puestoDto) {
        Puesto puesto = new Puesto(puestoDto);
        try {
            if (puesto.getPuesId() == null) {
                cbd.getEt().begin();
                cbd.getEm().persist(puesto);
                cbd.getEt().commit();
                new Message().show(Alert.AlertType.INFORMATION, "Guardar puesto", "Puesto guardado con exito");
            }
            else
            {
                cbd.getEt().begin();
                cbd.getEm().merge(puesto);
                cbd.getEt().commit();
                new Message().show(Alert.AlertType.INFORMATION, "Actualizar puesto", "Puesto actualizado con exito");
            }
        } catch (Exception ex) {
            new Message().show(Alert.AlertType.ERROR, "Guardar puesto", "Error al guardar el puesto\n" + ex.getMessage());
        }

    }

    public List<PuestoDto> BuscarPuesto(String nombre) {
        List<PuestoDto> puestoDto = new ArrayList();
        List<Puesto> puestos = new ArrayList();
        try {
            Query query = cbd.getEm().createNamedQuery("Puesto.findByPuesNombre", Puesto.class);
            query.setParameter("puesNombre", "%" + nombre + "%");
            puestos = query.getResultList();

            for (Puesto p : puestos) {
                puestoDto.add(new PuestoDto(p));
            }
            return puestoDto;
        } catch (NoResultException ex) {
            new Message().show(Alert.AlertType.ERROR, "Buscar Puesto", "No existen puestos en el registro");
        } catch (Exception ex) {
            new Message().show(Alert.AlertType.ERROR, "Buscar puesto", "Error al buscar el puesto" + ex.getMessage());
        }
        return null;
    }

    public void EliminarPuesto(Long id) {
        Puesto puesto = new Puesto();

        try {
            Query query = cbd.getEm().createNamedQuery("Puesto.findByPuesId", Puesto.class);
            query.setParameter("puesId", id);
            puesto = (Puesto) query.getSingleResult();

            cbd.getEt().begin();
            cbd.getEm().remove(puesto);
            cbd.getEt().commit();

        } catch (NoResultException ex) {
            new Message().show(Alert.AlertType.ERROR, "Eliminar Puesto", "No existe el empleado ha eliminar, verifique los datos");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                new Message().show(Alert.AlertType.ERROR, "Eliminar Puesto", "No se puede eliminar el puesto, esta siendo utilizado\npor otra instancia verifique e intentelo de nuevo");
            } else {
                new Message().show(Alert.AlertType.ERROR, "Eliminar Puesto", "Error al eliminar el puesto" + ex.getMessage());
            }
        }
    }

    public List<PuestoDto> getPuestos() {
        List<PuestoDto> puestoDto = new ArrayList();
        List<Puesto> puestos;
        try {
            Query query = cbd.getEm().createNamedQuery("Puesto.findAll", Puesto.class);
            puestos = query.getResultList();
            for (Puesto p : puestos) {
                puestoDto.add(new PuestoDto(p));
            }
            return puestoDto;
        } catch (NoResultException ex) {
            new Message().show(Alert.AlertType.ERROR, "", "No existen puestos en el registro");
        } catch (Exception ex) {
            new Message().show(Alert.AlertType.ERROR, "", ex.getMessage());
        }
        return null;
    }
}
