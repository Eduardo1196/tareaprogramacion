/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author liedu
 */
@Entity
@Table(name = "ASIGROL_ROL", schema = "UNA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rol.findAll", query = "SELECT r FROM Rol r")
    , @NamedQuery(name = "Rol.findByRolId", query = "SELECT r FROM Rol r WHERE r.rolId = :rolId")
    , @NamedQuery(name = "Rol.findByRolNombre", query = "SELECT r FROM Rol r WHERE r.rolNombre = :rolNombre")
    , @NamedQuery(name = "Rol.findByRolNumero", query = "SELECT r FROM Rol r WHERE r.rolNumero = :rolNumero")
    , @NamedQuery(name = "Rol.findByRolFechasemana", query = "SELECT r FROM Rol r WHERE r.rolFechasemana = :rolFechasemana")
    , @NamedQuery(name = "Rol.findByRolFijo", query = "SELECT r FROM Rol r WHERE r.rolFijo = :rolFijo")})
public class Rol implements Serializable {

    @Basic(optional = false)
    @Column(name = "ROL_NUMERO")
    private Integer rolNumero;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "ASIGROL_ROL_ID_GENERATOR", sequenceName = "una.ROL_SEQ03", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASIGROL_ROL_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "ROL_ID")
    private Long rolId;
    @Basic(optional = false)
    @Column(name = "ROL_NOMBRE")
    private String rolNombre;
    @Basic(optional = false)
    @Column(name = "ROL_FECHASEMANA")
    private String rolFechasemana;
    @Basic(optional = false)
    @Column(name = "ROL_FIJO")
    private String rolFijo;
    @OneToMany(mappedBy = "empRolId", fetch = FetchType.EAGER)
    private List<Empleado> empleadoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "diaRolId", fetch = FetchType.EAGER)
    private List<Dia> diaList;
    @JoinColumn(name = "ROL_PUES_ID", referencedColumnName = "PUES_ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private Puesto rolPuesId;

    public Rol() {
    }

    public Rol(Long rolId) {
        this.rolId = rolId;
    }

    public Rol(Long rolId, String rolNombre, Integer rolNumero, String rolFechasemana, String rolFijo) {
        this.rolId = rolId;
        this.rolNombre = rolNombre;
        this.rolNumero = rolNumero;
        this.rolFechasemana = rolFechasemana;
        this.rolFijo = rolFijo;
    }

    public Rol(RolDto rolDto) {
        this.rolId = rolDto.getId();
        actualizarRol(rolDto);
    }

    public void actualizarRol(RolDto rolDto) {
        this.rolNombre = rolDto.getNombre();
        this.rolNumero = rolDto.getNumero();
        this.rolFechasemana = rolDto.getFechaSemana();
        this.rolFijo = rolDto.getFjo();
    }

    public Long getRolId() {
        return rolId;
    }

    public void setRolId(Long rolId) {
        this.rolId = rolId;
    }

    public String getRolNombre() {
        return rolNombre;
    }

    public void setRolNombre(String rolNombre) {
        this.rolNombre = rolNombre;
    }

    public Integer getRolNumero() {
        return rolNumero;
    }

    public void setRolNumero(Integer rolNumero) {
        this.rolNumero = rolNumero;
    }

    public String getRolFechasemana() {
        return rolFechasemana;
    }

    public void setRolFechasemana(String rolFechasemana) {
        this.rolFechasemana = rolFechasemana;
    }

    public String getRolFijo() {
        return rolFijo;
    }

    public void setRolFijo(String rolFijo) {
        this.rolFijo = rolFijo;
    }

    @XmlTransient
    public List<Empleado> getEmpleadoList() {
        return empleadoList;
    }

    public void setEmpleadoList(List<Empleado> empleadoList) {
        this.empleadoList = empleadoList;
    }

    @XmlTransient
    public List<Dia> getDiaList() {
        return diaList;
    }

    public void setDiaList(List<Dia> diaList) {
        this.diaList = diaList;
    }

    public Puesto getRolPuesId() {
        return rolPuesId;
    }

    public void setRolPuesId(Puesto rolPuesId) {
        this.rolPuesId = rolPuesId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rolId != null ? rolId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rol)) {
            return false;
        }
        Rol other = (Rol) object;
        if ((this.rolId == null && other.rolId != null) || (this.rolId != null && !this.rolId.equals(other.rolId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tarea1.model.Rol[ rolId=" + rolId + " ]";
    }


}
