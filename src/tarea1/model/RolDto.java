/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.model;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author liedu
 */
public class RolDto {

    public SimpleStringProperty id;
    public SimpleStringProperty nombre;
    public SimpleIntegerProperty numero;
    public SimpleStringProperty fechaSemana;
    public SimpleStringProperty fjo; // modificar
    public PuestoDto puesto;
    public List<DiaDto> dias;

    public RolDto() {
        this.id = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
        this.numero = new SimpleIntegerProperty();
        this.fechaSemana = new SimpleStringProperty();
        this.fjo = new SimpleStringProperty();
        this.puesto = new PuestoDto();
        this.dias = new ArrayList<>();
    }
    
    public void  crearRol(){
        this.id = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
        this.numero = new SimpleIntegerProperty();
        this.fechaSemana = new SimpleStringProperty();
        this.fjo = new SimpleStringProperty();
         this.puesto = new PuestoDto();
        this.dias = new ArrayList<>();
    }

    public RolDto(Rol rol) {
        crearRol();
        this.id.set(rol.getRolId().toString());
        this.nombre.set(rol.getRolNombre());
        this.numero.set(rol.getRolNumero());
        this.fechaSemana.set(rol.getRolFechasemana());
        this.fjo.set(rol.getRolFijo());
        if(rol.getDiaList() != null){
            List<Dia> dias= new ArrayList<Dia>();
            dias = rol.getDiaList();
            for(Dia dia: dias){
                this.dias.add(new DiaDto(dia));
            }
        }
        //this.setPuesto(new PuestoDto(rol.getRolPuesId()));
    }

    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(this.id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public Integer getNumero() {
        return numero.get();
    }

    public void setNumero(Integer numero) {
        this.numero.set(numero);
    }

    public String getFechaSemana() {
        return fechaSemana.get();
    }

    public void setFechaSemana(String fechaSemana) {
        this.fechaSemana.set(fechaSemana);
    }

    public String getFjo() {
        return fjo.get();
    }

    public void setFjo(String fjo) {
        this.fjo.set(fjo);
    }

    public PuestoDto getPuesto() {
        return puesto;
    }

    public void setPuesto(PuestoDto puesto) {
        this.puesto = puesto;
    }

    public List<DiaDto> getDias() {
        return dias;
    }

    public void setDias(List<DiaDto> dias) {
        this.dias = dias;
    }
    
    

}
