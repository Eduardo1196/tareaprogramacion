/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author liedu
 */
@Entity
@Table(name = "ASIGROL_EMPLEADO", schema = "UNA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empleado.findAll", query = "SELECT e FROM Empleado e")
    , @NamedQuery(name = "Empleado.findByEmpId", query = "SELECT e FROM Empleado e WHERE e.empId = :empId")
    , @NamedQuery(name = "Empleado.findByEmpNombre", query = "SELECT e FROM Empleado e WHERE e.empNombre like :empNombre")
    , @NamedQuery(name = "Empleado.findByEmpApellidos", query = "SELECT e FROM Empleado e WHERE e.empApellidos = :empApellidos")
    , @NamedQuery(name = "Empleado.findByEmpCedula", query = "SELECT e FROM Empleado e WHERE e.empCedula = :empCedula")
    , @NamedQuery(name = "Empleado.findByEmpFolio", query = "SELECT e FROM Empleado e WHERE e.empFolio = :empFolio")
    , @NamedQuery(name = "Empleado.findByEmpCorreo", query = "SELECT e FROM Empleado e WHERE e.empCorreo = :empCorreo")})
public class Empleado implements Serializable {

    @Basic(optional = false)
    @Column(name = "EMP_FOLIO")
    private Integer empFolio;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "ASIGROL_EMPLEADO_ID_GENERATOR", sequenceName = "una.EMPLEADO_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASIGROL_EMPLEADO_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "EMP_ID")
    private Long empId;
    @Basic(optional = false)
    @Column(name = "EMP_NOMBRE")
    private String empNombre;
    @Basic(optional = false)
    @Column(name = "EMP_APELLIDOS")
    private String empApellidos;
    @Basic(optional = false)
    @Column(name = "EMP_CEDULA")
    private String empCedula;
    @Basic(optional = false)
    @Column(name = "EMP_CORREO")
    private String empCorreo;
    @JoinColumn(name = "EMP_PUES_ID", referencedColumnName = "PUES_ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private Puesto empPuesId;
    @JoinColumn(name = "EMP_ROL_ID", referencedColumnName = "ROL_ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private Rol empRolId;

    public Empleado() {
    }

    public Empleado(Long empId) {
        this.empId = empId;
    }

    public Empleado(Long empId, String empNombre, String empApellidos, String empCedula, Integer empFolio, String empCorreo) {
        this.empId = empId;
        this.empNombre = empNombre;
        this.empApellidos = empApellidos;
        this.empCedula = empCedula;
        this.empFolio = empFolio;
        this.empCorreo = empCorreo;
    }

    public Empleado(EmpleadoDto empleadoDto) {
        this.empId = empleadoDto.getId();
        actualizarEmpleado(empleadoDto);
    }

    public void actualizarEmpleado(EmpleadoDto empleadoDto) {
        this.empNombre = empleadoDto.getNombre();
        this.empApellidos = empleadoDto.getApellidos();
        this.empCedula = empleadoDto.getCedula();
        this.empFolio = empleadoDto.getFolio();
        this.empCorreo = empleadoDto.getCorreo();
    }

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public String getEmpNombre() {
        return empNombre;
    }

    public void setEmpNombre(String empNombre) {
        this.empNombre = empNombre;
    }

    public String getEmpApellidos() {
        return empApellidos;
    }

    public void setEmpApellidos(String empApellidos) {
        this.empApellidos = empApellidos;
    }

    public String getEmpCedula() {
        return empCedula;
    }

    public void setEmpCedula(String empCedula) {
        this.empCedula = empCedula;
    }

    public Integer getEmpFolio() {
        return empFolio;
    }

    public void setEmpFolio(Integer empFolio) {
        this.empFolio = empFolio;
    }

    public String getEmpCorreo() {
        return empCorreo;
    }

    public void setEmpCorreo(String empCorreo) {
        this.empCorreo = empCorreo;
    }

    public Puesto getEmpPuesId() {
        return empPuesId;
    }

    public void setEmpPuesId(Puesto empPuesId) {
        this.empPuesId = empPuesId;
    }

    public Rol getEmpRolId() {
        return empRolId;
    }

    public void setEmpRolId(Rol empRolId) {
        this.empRolId = empRolId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (empId != null ? empId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleado)) {
            return false;
        }
        Empleado other = (Empleado) object;
        if ((this.empId == null && other.empId != null) || (this.empId != null && !this.empId.equals(other.empId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tarea1.model.Empleado[ empId=" + empId + " ]";
    }


}
